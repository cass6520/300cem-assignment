package com.example.android_assignment1

import android.content.Context
import com.example.android_assignment1.map.MapContact
import com.example.android_assignment1.map.MapRepository
import com.example.android_assignment1.map.MapsPresenter
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class MapPresenterTest {

    lateinit var mapsPresenter: MapsPresenter

    @RelaxedMockK
    lateinit var mapRepository: MapRepository

    @RelaxedMockK
    lateinit var view: MapContact.mapView

    @Before
    fun SetUp() {
        MockKAnnotations.init(this)
        mapsPresenter = MapsPresenter(view, mapRepository)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun testDisplayMessage() {
        // Given
        val mockData = "Toast Message"
        // When
        mapsPresenter.displayToastMessageOnView(mockData)
        // Then
        verify {
            view.displayToastMessage(mockData)
        }
    }
}
