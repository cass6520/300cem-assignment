package com.example.android_assignment1

import android.content.Context
import com.example.android_assignment1.map.MapContact
import com.example.android_assignment1.map.MapRepository
import com.example.android_assignment1.map.MapsPresenter
import com.example.android_assignment1.shop.Shop
import com.example.android_assignment1.shop.ShopPresenter
import com.example.android_assignment1.shop.ShopRepository
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class ShopPresenterTest {

    lateinit var shopPresenter: ShopPresenter

    @RelaxedMockK
    lateinit var shopRepository: ShopRepository

    @RelaxedMockK
    lateinit var view: Shop

    @Before
    fun SetUp() {
        MockKAnnotations.init(this)
        shopPresenter = ShopPresenter(view)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun testDisplayMessage() {
        // Given
        val mockData = "Toast Message"
        // When
        shopPresenter.displayToastMessageOnView(mockData)
        // Then
        verify {
            view.displayToastMessage(mockData)
        }
    }
}
