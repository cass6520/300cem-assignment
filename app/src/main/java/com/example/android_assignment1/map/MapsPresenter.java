package com.example.android_assignment1.map;

import com.google.android.gms.maps.model.LatLng;

public class MapsPresenter implements MapContact.Presenter {
    public MapContact.mapView mapsView;
    public MapRepository mapRepository;

    public MapsPresenter(MapContact.mapView mapsView, MapRepository mapRepository) {
        this.mapsView = mapsView;
        this.mapRepository = mapRepository;
    }

    @Override
    public void onMapReady() {
        mapRepository.loadLocation(this);
    }

    @Override
    public void getLocation(String licensePlate, Location location) {
        LatLng locationToLatLng = new LatLng(location.getLat(), location.getLng());
        mapsView.addScooter(licensePlate, locationToLatLng);
    }

    //Testing
    @Override
    public void displayToastMessageOnView(String msg) {mapsView.displayToastMessage(msg); }

}
