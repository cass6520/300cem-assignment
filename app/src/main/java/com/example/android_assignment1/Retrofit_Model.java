package com.example.android_assignment1;

import com.google.gson.annotations.SerializedName;

public class Retrofit_Model {

    @SerializedName("_id")
    public String ID;
    @SerializedName("name_eng")
    public String name_eng;
    @SerializedName("name_chin")
    public String name_chin;
    @SerializedName("address_eng")
    public String  address_eng;
    @SerializedName("address_chin")
    public String address_chin;
    @SerializedName("latitude")
    public String latitude;
    @SerializedName("longitude")
    public String longitude;
    @SerializedName("storephoto")
    public String storephoto;

    public Retrofit_Model(){}
    public Retrofit_Model(String id, String engName, String chinName, String engAddress, String chinAddress,
                          String latitude, String longitude, String storePhoto)
    {
        this.ID = id;
        this.name_eng = engName;
        this.name_chin = chinName;
        this.address_eng = engAddress;
        this.address_chin = chinAddress;
        this.latitude = latitude;
        this.longitude = longitude;
        this.storephoto = storePhoto;
    }

    public void setID(String id) {this.ID = id;}
    public void setName_eng(String value){this.name_eng = value;}
    public void setName_chin(String value){this.name_chin = value;}
    public void setAddress_eng(String value){this.address_eng = value;}
    public void setAddress_chin(String value){this.address_chin = value;}
    public void setLatitude(String value){this.latitude = value;}
    public void setLongitude(String value){this.longitude = value;}
    public void setStorephoto(String value){this.storephoto = value;}

    public String getID(){return this.ID;}
    public String getName_eng(){return this.name_eng;}
    public String getName_chin(){return this.name_chin;}
    public String getAddress_eng(){return this.address_eng;}
    public String getAddress_chin(){return this.address_chin;}
    public String getLatitude(){return this.latitude;}
    public String getLongitude(){return this.longitude;}
    public String getStorephoto(){return this.storephoto;}
}

