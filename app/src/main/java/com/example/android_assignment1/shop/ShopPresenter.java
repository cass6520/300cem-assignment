package com.example.android_assignment1.shop;

import com.example.android_assignment1.Retrofit_Model;

import java.util.List;
import retrofit2.Response;



public class ShopPresenter {
    private ShopRepository shopRepository;
    private View view;


    public ShopPresenter(View view){
        this.view = view;
        this.shopRepository = new ShopRepository();
    }

    public void callAPI() {
        //Show all record
        //Call
        shopRepository.API(this);
//        System.out.println("Call API");
    }

    public void getResponseSuccess(List<Retrofit_Model> body){
        view.addToView(body);
    }

    public interface View {
        void addToView(List<Retrofit_Model> list);
        void displayToastMessage(String msg);
    }

    public void displayToastMessageOnView(String msg) {view.displayToastMessage(msg); }
}
