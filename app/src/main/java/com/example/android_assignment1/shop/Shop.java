package com.example.android_assignment1.shop;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_assignment1.MainActivity;
import com.example.android_assignment1.map.MapsActivity;

import com.example.android_assignment1.R;
import com.example.android_assignment1.Retrofit_Model;
import com.example.android_assignment1.qrcode.QRCodeActivity;

import java.util.List;

public class Shop extends AppCompatActivity implements ShopPresenter.View{
    private RecyclerView recycler_view;
    private ShopRecyclerViewAdapter adapter;
    private ShopPresenter shopPresenter;
    //Nav bar
    public Button btnHone, btnShop, btnMap, btnMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        shopPresenter = new ShopPresenter(this);
        shopPresenter.callAPI();

        //Nav bar
        btnHone = findViewById(R.id.btnHome);
        btnShop = findViewById(R.id.btnShop);
        btnMap = findViewById(R.id.btnMap);
        btnMenu = findViewById(R.id.btnMenu);

        //RecyclerView
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view_shopInfo);
    }

    @Override
    public void addToView(List<Retrofit_Model> list) {
        //Set as list
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        //Set grid
        recycler_view.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        //To adapter
        adapter = new ShopRecyclerViewAdapter(list, shopPresenter, this);
        //Adapter to RecyclerView
        recycler_view.setAdapter(adapter);
    }

    //Testing
    @Override
    public void displayToastMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    //Nav bar
    public void ToHome(View view) {
        Intent i = new Intent(Shop.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    public void ToShop(View view) {
        finish();
        startActivity(getIntent());
    }

    public void ToMap(View view) {
        Intent i = new Intent(Shop.this, MapsActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    public void ToNutLab(View view) {
        Intent i = new Intent(Shop.this, QRCodeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }
}