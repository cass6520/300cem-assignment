package com.example.android_assignment1.shop;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.android_assignment1.R;

import java.util.List;
import java.util.Locale;

import com.example.android_assignment1.Retrofit_Model;

public class ShopRecyclerViewAdapter extends RecyclerView.Adapter<ShopRecyclerViewAdapter.ViewHolder>{

    private List<Retrofit_Model> mData;
    ShopPresenter shopPresenter;
    Context mContext;

    ShopRecyclerViewAdapter(List<Retrofit_Model> data, ShopPresenter infoPresenter, Context context) {
        mData = data;
        shopPresenter = infoPresenter;
        mContext = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView store_photo;
        private TextView store_name, store_address;

        ViewHolder(View itemView) {
            super(itemView);
            store_name = (TextView) itemView.findViewById(R.id.store_name);
            store_address = (TextView) itemView.findViewById(R.id.store_address);
            store_photo = (ImageView) itemView.findViewById(R.id.store_photo);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Link the shop_list
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_shop_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Retrofit_Model obj = mData.get(position);
//        System.out.println("mData: " + mData);
//        mStoreInfo.add(obj.ID);

//        System.out.println(Locale.getDefault().getDisplayLanguage());
        //Use glide for URL to photo
        Glide.with(mContext).load(obj.storephoto).into(holder.store_photo);
        //Check the language of the device
        if (Locale.getDefault().getDisplayLanguage().equals("中文")) {
            //Chinese
            holder.store_name.setText(obj.name_chin);
            holder.store_address.setText(obj.address_chin);
       } else {
            //English
            holder.store_name.setText(obj.name_eng);
            holder.store_address.setText(obj.address_eng);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}